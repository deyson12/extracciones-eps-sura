import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Main {
	
	int countfila = 0;
	
	public static void main(String[] args) {
		try {
			Main m = new Main();
			m.readFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void readFile() throws Exception {

		String[] files = { "AyudasDx_20190702", "Formulas_20190702", "Remisiones_20190702" };
		String[] filesS = { "*ENDLINE*", "|*ENDLINE", "*ENDLINE*" };

		String[] tituloAyudas = { "FECHACREACION", "FECHAIMPRESION", "AUTORIZACION", "TIPOIDENTIFICACION",
				"IDENTIFICACIONPACIENTE", "CODIPSGENERA", "NOMBREIPSGENERA", "CODIPSIMPRIME", "NOMBREIPSIMPRIME",
				"TIPOIDENTIFICACIONCREAORDEN", "IDENTIFICACIONCREAORDEN", "PRIMERNOMBRECREAORDEN",
				"PRIMERAPELLIDOCREAORDEN", "ROLGENERA", "DESCRIPCIONROLGENERA", "TIPOIDENTIFICACIONIMPRIME",
				"IDENTIFICACIONIMPRIME", "PRIMERNOMBREIMPRIME", "PRIMERAPELLIDOIMPRIME", "ROLIMPRIME",
				"DESCRICIPCIONROLIMPRIME", "TIPOCOBRO" };

		String[] tituloFormulas = { "FECHACREACION", "FECHAIMPRESION", "AUTORIZACION", "TIPOIDENTIFICACION",
				"IDENTIFICACIONPACIENTE", "CODIPSGENERA", "NOMBREIPSGENERA", "CODIPSIMPRIME", "NOMBREIPSIMPRIME",
				"TIPOIDENTIFICACIONCREAORDEN", "IDENTIFICACIONCREAORDEN", "PRIMERNOMBRECREAORDEN",
				"PRIMERAPELLIDOCREAORDEN", "ROLGENERA", "DESCRIPCIONROLGENERA", "TIPOIDENTIFICACIONIMPRIME",
				"IDENTIFICACIONIMPRIME", "PRIMERNOMBREIMPRIME", "PRIMERAPELLIDOIMPRIME", "ROLIMPRIME",
				"DESCRICIPCIONROLIMPRIME", "TIPOCOBRO", "RAFENTREGAACTUAL" };

		String[] tituloRemisiones = { "FECHACREACION", "FECHAIMPRESION", "AUTORIZACION", "TIPOIDENTIFICACION",
				"IDENTIFICACIONPACIENTE", "CODIPSGENERA", "NOMBREIPSGENERA", "CODIPSIMPRIME", "NOMBREIPSIMPRIME",
				"TIPOIDENTIFICACIONCREAORDEN", "IDENTIFICACIONCREAORDEN", "PRIMERNOMBRECREAORDEN",
				"PRIMERAPELLIDOCREAORDEN", "ROLGENERA", "DESCRIPCIONROLGENERA", "TIPOIDENTIFICACIONIMPRIME",
				"IDENTIFICACIONIMPRIME", "PRIMERNOMBREIMPRIME", "PRIMERAPELLIDOIMPRIME", "ROLIMPRIME",
				"DESCRICIPCIONROLIMPRIME", "CODIGOPRESTACION", "NOMBREPRESTACION", "TIPOCOBRO" };

		for (int i = 0; i < files.length; i++) {
			
			countfila = 0;
			XSSFWorkbook libro = new XSSFWorkbook();
			XSSFSheet hoja = libro.createSheet();
			
			String fileName = files[i];
			System.out.println("Empieza: " + fileName);
			String salto = filesS[i];

			File file = new File(
					"C:\\Users\\deysespo\\Desktop\\Sprints\\EPS\\Extracciones\\Profesional\\20190702\\Resultados\\"
							+ fileName + ".txt");
			
			FileOutputStream elFichero = new FileOutputStream("C:\\Users\\deysespo\\Desktop\\Sprints\\EPS\\Extracciones\\Profesional\\20190702\\Resultados\\"
					+ fileName + ".xlsx");
			
			FileWriter fw=new FileWriter("C:\\Users\\deysespo\\Desktop\\Sprints\\EPS\\Extracciones\\Profesional\\20190702\\Resultados\\"
					+ fileName + "_new.txt");   

			BufferedReader br = new BufferedReader(new FileReader(file));

			String st;
			
			String[] titulos = null;
			if(fileName.contains("AyudasDx")) {
				titulos = tituloAyudas;
			}else if (fileName.contains("Formulas")) {
				titulos = tituloFormulas;
			}else if (fileName.contains("Remisiones")) {
				titulos = tituloRemisiones;
			}
			
			//Agregamos los titlulos
			XSSFRow fila = hoja.createRow(countfila);
			for (int j = 0; j < titulos.length; j++) {
				XSSFCell celda = fila.createCell((short) j);
				XSSFRichTextString texto = new XSSFRichTextString(titulos[j]);
				celda.setCellValue(texto);
			}
			countfila++;
			
			//Agregamos los valores
			boolean reset = false;
			String linea = "";
			while ((st = br.readLine()) != null) {
				if(reset) {
					linea = "";
					reset = false;
				}
				if(st.isEmpty()) {
					linea += st.replace(salto, "").replace("|", "%AQUI%");
					String[] celdaS = linea.split("%AQUI%");
					escribirCelda(hoja, titulos, celdaS);
					reset = true;
					fw.write(linea.replace("%AQUI%", "|")+"\n");
				}else {
					linea += st.replace("|", "%AQUI%");
				}
			}
			System.out.println("Total "+fileName+": " + countfila);
			fw.close();    
			br.close();
			
			libro.write(elFichero);
			elFichero.close();
			System.out.println("Termina: " + fileName);
		}
	}

	private void escribirCelda(XSSFSheet hoja, String[] titulos, String[] celdaS) {
		XSSFRow fila = hoja.createRow(countfila);
		
		for (int j = 0; j < celdaS.length; j++) {
			try {
				XSSFCell celda = fila.createCell((short) j);
				XSSFRichTextString texto = new XSSFRichTextString(celdaS[j]);
				celda.setCellValue(texto);
			}catch (Exception e) {
				System.out.println("Datos: " + j + ", Texto: " + celdaS[j]);
				throw e;
			}
		}
		countfila++;
	}
}
